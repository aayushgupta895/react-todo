import { useState } from "react";
import List from "./List";
import "./App.css";

function App() {
  const [task, setTask] = useState("");
  const [maxId, setMaxId] = useState(3);
  const [todos, setTodos] = useState({});

  const addTask = () => {
    const newObj = { ...todos };
    newObj[maxId] = {};
    if (task == "") return;
    newObj[maxId]["task"] = task;
    setTodos(newObj);
    setMaxId((old) => old + 1);
    setTask("");
  };

  return (
    <>
      <div className="container">
        <h1>Todo</h1>
        <div className="inputContainer">
          <input
            type="text"
            id="inputTask"
            placeholder="write your task here"
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
          <button className="submit" onClick={() => addTask()}>
            add
          </button>
        </div>
        <hr />
        <div className="tasks">
          <List todos={todos} setTodos={setTodos} />
        </div>
        {Object.keys(todos).length == 0 && (
          <h3 className="instruction">add your task here</h3>
        )}
      </div>
    </>
  );
}

export default App;
