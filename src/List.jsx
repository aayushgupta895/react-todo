import React, { useState } from "react";
import "./App.css";

function List(props) {
  const { todos, setTodos } = props;

  const [editableId, setEditableId] = useState([]);
  const [editTasks, setEditTasks] = useState({});
  const [isChecked, setIsChecked] = useState({});

  function deleteTask(id) {
    const newObj = { ...todos };
    delete newObj[id];
    setTodos(newObj);
  }

  function editTask(id) {
    console.log(`here`);
    setEditableId((old) => [...old, id]);
    const newEditTasks = { ...editTasks };
    newEditTasks[id] = todos[id];
    setEditTasks(newEditTasks);
  }

  function saveTask(id) {
    const newObj = { ...todos };
    newObj[id] = editTasks[id];
    setTodos(newObj);
    const newEditableId = editableId.filter((editId) => {
      return editId != id;
    });
    setEditableId(newEditableId);
  }

  function handleOnChange(e, key) {
    const newObj = { ...editTasks };
    newObj[key] = {};
    newObj[key]["task"] = e.target.value;
    setEditTasks(newObj);
  }

  function handleCheckbox(event, key) {
    const newObj = { ...isChecked };
    newObj[key] = event.target.checked;
    setIsChecked(newObj);
  }

  return (
    <>
      {Object.keys(todos).map((key) => {
        return (
          <div key={key}>
            <input
              type="checkbox"
              checked={isChecked[key] || false}
              onChange={(e) => handleCheckbox(e, key)}
            />
            {!editableId.includes(key) ? (
              <div className={`task ${isChecked[key] ? "check" : "uncheck"}`}>
                {todos[key]["task"]}
              </div>
            ) : (
              <input
                className="task uncheck"
                type="text"
                onChange={(e) => handleOnChange(e, key)}
                value={editTasks[key]["task"]}
              />
            )}
            <button className="delete-btn" onClick={() => deleteTask(key)}>
              Delete
            </button>
            <button className="edit-btn" onClick={() => editTask(key)}>
              Edit
            </button>
            <button className="save-btn" onClick={() => saveTask(key)}>
              Save
            </button>
          </div>
        );
      })}
    </>
  );
}

export default List;
